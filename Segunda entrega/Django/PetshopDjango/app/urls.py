from django.conf.urls import url
from django.contrib import admin
from django.urls import path,re_path, include
from .views import catalogo, contacto, inicio, registro, agregar_prod, modificar_prod, eliminar_lista , eliminar_prod, producto_list
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.urls import reverse_lazy

urlpatterns = [
    path('', inicio, name="inicio"),
    path('catalogo/', producto_list, name="catalogo"),
    path('contacto/', contacto,  name="contacto"),
    path('registro/', registro, name="registro"),
    path('agregar-producto/', agregar_prod, name="agregar"),
    path('modificar/<id>/', modificar_prod, name="modificar"),
    path('eliminar', eliminar_lista, name="eliminar"),
    path('eliminar-producto/<id>/', eliminar_prod, name="eliminar-producto"),
    path('', LoginView.as_view(template_name='index.html', success_url=reverse_lazy('adopcion:solicitud_listar')), name = 'login'),
    path('reset/password_reset', PasswordResetView.as_view(template_name='registration/password_reset_forms.html', 
    email_template_name="registration/password_reset_email.html"), name = 'password_reset'),
    path('reset/password_reset_done', PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'), 
    name = 'password_reset_done'),
    re_path(r'^reset/(?P<uidb64>[0-9A-za-z_\-]+)/(?P<token>.+)/$', PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirms.html'), 
    name = 'password_reset_confirm'),
    path('reset/done',PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html') , 
    name = 'password_reset_complete'),
    path('oauth/', include('social_django.urls', namespace='social')),
    
    

]

from django.db.models import fields
import django_filters
from .models import Producto


class SnippetFilter(django_filters.FilterSet):
    class Meta:
        model = Producto
        fields = ('nombre' , 'animal')
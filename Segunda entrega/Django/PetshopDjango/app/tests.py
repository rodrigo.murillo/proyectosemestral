from django.test import TestCase
from .models import Producto
import unittest

class CommunityTestCase(unittest.TestCase):
    # @classmethod
    def setUp(self):
        # create demography object
        d2 = Producto.objects.create(descripcion="Bocaditos de entrenamiento, 70gr.")
        # add this object into Community & create a Community post
        Producto.objects.create(nombre="Goffy", descripcion=d2,
                                 precio="hola",imagen="This is a image")

    # @classmethod
    def test_add_post(self):
        # get Community object
        post1 = Producto.objects.get(nombre="Goffy")
        self.assertEqual(post1.descripcion.name, "Bocaditos de entrenamiento, 70gr.")

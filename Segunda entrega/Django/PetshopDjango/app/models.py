from django.db import models

# Create your models here.


class Animal(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class Producto(models.Model):
    nombre = models.CharField(max_length=70)
    precio = models.IntegerField()
    descripcion = models.CharField(max_length=200)
    animal = models.ForeignKey(Animal, on_delete=models.PROTECT)
    imagen = models.ImageField(upload_to="Media/productos", null=True)

    def __str__(self):
        return self.nombre

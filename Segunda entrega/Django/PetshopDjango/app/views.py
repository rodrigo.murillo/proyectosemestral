from django.forms.widgets import MediaDefiningClass
from django.http.response import Http404
from django.shortcuts import redirect, render, redirect, get_object_or_404
from .models import Producto
from .forms import CustomUserCreationForm, productoform
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.core.paginator import Paginator
from django.http import Http404
# Create your views here.


def inicio(request):
    return render(request, 'app/inicio.html')


def catalogo(request):
    # aqui tira un error pero el visual studio code lo toma igual por que no lee el objects
    productos = Producto.objects.all()
    page = request.GET.get('page', 1)
    
    try:
        paginator = Paginator(productos,12)
        productos = paginator.page(page)

    except:
        raise Http404 

    data = {
        'entity': productos,
        'paginator': paginator

    }


    return render(request, 'app/catalogo.html', data)
 

def contacto(request):
    return render(request, 'app/contacto.html')


def agregar_prod(request):

    data = {
        'form' : productoform()
    }

    if request.method == 'POST':
        formulario = productoform(data = request.POST, files = request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Producto Registrado correctamente")
        else:
            data["form"] = formulario

    return render(request, 'producto/agregar.html', data)

def modificar_prod(request, id):

    producto = get_object_or_404(Producto, id=id)

    data = {
        'form' : productoform(instance=producto)
    }

    if request.method == 'POST':
        formulario = productoform(data=request.POST, instance=producto, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Lo lograste putitia")
            return redirect(to="catalogo")
        data["form"] = formulario

    return render(request, 'Producto/modificar.html', data)


def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            # estas dos lineas sirver para poder logearse automaticamente despues de registrarse
            # user = authenticate(username=formulario.cleaned_data("username"), password=formulario.cleaned_data("password1"))
            # login(request, user)
            messages.success(request, "Te has registrado correctamente")
            return redirect(to="inicio")
        data["form"] = formulario

    return render(request, 'registration/registro.html', data)

def eliminar_lista (request):

    lista = Producto.objects.all()

    data = {
        'productos' : lista
    }

    return render(request, 'Producto/eliminar.html', data)

def eliminar_prod (request, id):

    producto = get_object_or_404(Producto, id=id)
    producto.delete()
    return redirect(to="catalogo")


def producto_list(request):
    qs = Producto.objects.all()
    page = request.GET.get('page', 1)
    id_exact_query = request.GET.get('id_exact')
    nombre_query = request.GET.get('nombre')
    precio_query = request.GET.get('precio')
    

    if id_exact_query != '' and id_exact_query is not None:
        qs = qs.filter(id=id_exact_query)

    elif nombre_query != '' and nombre_query is not None:
        qs = qs.filter(nombre__icontains=nombre_query)

    elif precio_query != '' and precio_query is not None:
        qs = qs.filter(precio__icontains=precio_query)

    try:
        paginator = Paginator(qs,12)
        qs = paginator.page(page)

    except:
        raise Http404 

    context = {

        'entity': qs,
        'paginator': paginator,
        'queryset': qs
    }

    return render(request, "app/catalogo.html", context)
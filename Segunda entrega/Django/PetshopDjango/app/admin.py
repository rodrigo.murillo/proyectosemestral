from django.contrib import admin
from .models import Animal, Producto

# Register your models here.


admin.site.register(Animal)
admin.site.register(Producto)
